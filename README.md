# Git-Dependency

<p align="center">
    <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" />
    <img src="https://img.shields.io/badge/Maven%20Central-0.02-brightgreen" />
    <img src="https://img.shields.io/:License-apache-yellowgreen.svg" />
   
</p>


## 快速使用 

## 场景（一）

假设我们有一个git仓库，想使用它构建出来的jar，但这个git又没有发布到maven仓库中。这时，我们的常规做法是clone这个git到本地，然后打包并部署到本地maven仓库，然后再使用它。

现在有了这个插件，我们可以这样操作：

1. 在我们项目pom.xml文件中的build->plugins中增加```git-dependency```插件，如下：

```xml 
<build>
	<plugins>
		<plugin>
			<groupId>com.gitee.hedingwei</groupId>
			<artifactId>git-dependency</artifactId>
			<version>0.0.2</version>
			<configuration>
				<sourceDir>./git-deps/sources</sourceDir>
				<useSystemMavenRepository>true</useSystemMavenRepository>
				<gitDependencies>
					<Dependency>
						<gitUrl>https://gitee.com/hedingwei/yunxin-utils.git</gitUrl>
						<branch>refs/tags/0.0.2</branch>
					</Dependency>
				</gitDependencies>
			</configuration>
		</plugin>
	</plugins>
</build>

```

如果你用的是idea，则在maven的plugins里面点击git-dependency/git-dependency，如下图：

<img src="https://images.gitee.com/uploads/images/2021/0624/142514_113fa94b_341908.png" width="480" />


也可以直接命令行：
```sh
mvn git-dependency:git-dependency
```
然后本插件就会自动去拉去指定的git，然后默认```mvn clean install```，将构建好的jar安装到本地仓库里面去，这样在项目的依赖里面就可以正常写Dependency了，比如本例子中的``` https://gitee.com/hedingwei/yunxin-utils.git ```对于的maven坐标就是：

```xml
   <dependency>
	<groupId>com.yunxin.utils</groupId>
	<artifactId>commons</artifactId>
	<version>1.0-SNAPSHOT</version>
    </dependency>
```


