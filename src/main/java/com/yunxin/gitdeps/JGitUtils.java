package com.yunxin.gitdeps;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.util.Arrays;

public class JGitUtils {

    public static void clone(String url, String branch,String username, String password,String localPath){
        try {
            Git r = Git.cloneRepository()
                    .setDirectory(new File(localPath))
                    .setURI(url)
                    .setBranchesToClone(Arrays.asList(branch)).setNoCheckout(true)
                    .call();

        } catch (GitAPIException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {




    }
}
