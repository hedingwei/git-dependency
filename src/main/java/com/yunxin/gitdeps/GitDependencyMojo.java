package com.yunxin.gitdeps;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.invoker.*;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Mojo(name = "git-dependency", defaultPhase = LifecyclePhase.GENERATE_RESOURCES )
public class GitDependencyMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    MavenProject project;

    @Parameter(property = "sourceDir")
    String sourceDir;

    @Parameter(property = "repositoryDir")
    String repositoryDir;

    @Parameter(property = "useSystemMavenRepository", defaultValue = "true")
    Boolean useSystemMavenRepository = true;

    @Parameter(property = "alwaysClean", defaultValue = "true")
    Boolean alwaysClean = true;

    @Parameter
    List<Dependency> gitDependencies;

    public void initDirectories(){
        File sources = getSourceDir();
        File repositoryDir = getRepositoryDir();
        if(!sources.exists()){
            sources.mkdirs();
        }

        if(!repositoryDir.exists()){
            repositoryDir.mkdirs();
        }

    }



    public void processDependency(Dependency dependency) throws  Throwable{
        {
            Git r = null;
            boolean alreadyExist = false;

            String url = dependency.getGitUrl();
            File projectSourceLocalPath = resolveLocalSourcesPathForGitUrl(url);
            String branch;
            if(dependency.getBranch()==null){
                branch = "refs/heads/master";
            }else{
                branch = dependency.getBranch();
            }

            try {
                CloneCommand cloneCommand = Git.cloneRepository();
                cloneCommand.setDirectory(projectSourceLocalPath);
                cloneCommand.setURI(url);
                cloneCommand.setBranchesToClone(Arrays.asList(branch));
                cloneCommand.setNoCheckout(false);
                if((dependency.getUsername()!=null)&&(dependency.getPassword()!=null)){
                    cloneCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(dependency.getUsername(),dependency.getPassword()));
                }
                cloneCommand.call();

            } catch (Throwable e) {
                System.out.println(e.getMessage());
                if(e.getMessage()!=null &&(e.getMessage().contains("already exists"))){
                    alreadyExist = true;
                }else{
                    e.printStackTrace();
                }
            }
            if(alreadyExist){
                try {
                    r= Git.open(projectSourceLocalPath);
                    PullCommand pullCommand = r.pull()
                            .setRemoteBranchName(branch);
                    if((dependency.getUsername()!=null)&&(dependency.getPassword()!=null)){
                        pullCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(dependency.getUsername(),dependency.getPassword()));
                    }
                    PullResult pullResult = pullCommand.call();
                    System.out.println(pullResult);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            try{
                InvocationRequest request = new DefaultInvocationRequest();
                request.setPomFile( new File( projectSourceLocalPath,"pom.xml" ) );
                request.setBaseDirectory(projectSourceLocalPath);

                getLog().info("==>"+dependency);
                if(dependency.getBuildCmd()==null){

                    useDefaultInvocationRequest(request);
                }else{
                    getLog().info("customized cmd: "+dependency.getBuildCmd());
                   List<String> list = Arrays
                           .stream(dependency.getBuildCmd().split(" "))
                           .filter(s->(s!=null)&&(!s.isEmpty()))
                           .collect(Collectors.toList());
                    if(!useSystemMavenRepository){
                        request.setLocalRepositoryDirectory(getRepositoryDir());
                    }
                    request.setBatchMode(true);
                    request.setAlsoMake(true);
                    request.setAlsoMakeDependents(true);
                    request.setGoals(list);
                }

                Invoker invoker = new DefaultInvoker();
                InvocationResult invocationResult = invoker.execute( request );

            }catch (Throwable t){
                t.printStackTrace();
            }

        }
    }

    public void execute() throws MojoExecutionException, MojoFailureException {

        initDirectories();


        for (Dependency gitDependency : gitDependencies) {
            try {
                processDependency(gitDependency);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                throw new MojoFailureException(throwable.getMessage(),throwable);
            }
        }


    }
    // ...

    public File getSourceDir(){
        if(sourceDir==null){
            sourceDir = System.getProperty("user.home")+File.separator+"git-dependency"+File.separator+"sources";
        }
        return new File(sourceDir);
    }

    public File getRepositoryDir(){
        if(repositoryDir==null){
            repositoryDir = System.getProperty("user.home")+File.separator+"git-dependency"+File.separator+"repository";
        }
        return new File(repositoryDir);
    }

    public File resolveLocalSourcesPathForGitUrl(String gitUrl){
        URI uri = URI.create(gitUrl);
        File localPath = new File(getSourceDir(),uri.getHost()+File.separator+uri.getPath().replace(".git",""));
        return localPath;
    }

    public InvocationRequest useDefaultInvocationRequest(InvocationRequest request){

        if(!useSystemMavenRepository){
            request.setLocalRepositoryDirectory(getRepositoryDir());
        }

        request.setBatchMode(true);
        request.setAlsoMake(true);
        request.setAlsoMakeDependents(true);
        Properties properties = new Properties();
        properties.setProperty("skipTests", "true");
        request.setProperties(properties);
        if(alwaysClean){
            request.setGoals( Arrays.asList( "clean", "install"));
        }else{
            request.setGoals( Arrays.asList( "install"));
        }

        return request;
    }



}
